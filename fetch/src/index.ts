import fetch from "node-fetch";
import {CepDTO} from "./cepdto.js";
import {PostDTO} from "./postdto.js";

try {
	const uriBase = "https://viacep.com.br/ws";
	const cep = "56780000";
	const formato = "json";
	const resposta = await fetch(`${uriBase}/${cep}/${formato}`);
	if (resposta.ok) {
		const dadosJSON: CepDTO = (await resposta.json()) as CepDTO;
		console.log("Dados: ", dadosJSON);
	} else {
		console.log(`Status: ${resposta.status}`);
		console.log(`StatusText: ${resposta.statusText}`);
	}
} catch (error) {
	console.log("Falha de acesso ao web services: /n", error);
}

try {
	const uri = "https://jsonplaceholder.typicode.com";
	//Realizar um get
	/*
	const id = 1;
	const resposta = await fetch(`${uri}/posts/${id}`);
	if (resposta.ok) {
		const dadosJSON: PostDTO = (await resposta.json()) as PostDTO;
		console.log(dadosJSON);
	} else {
		console.log(`Status: ${resposta.status}`);
		console.log(`StatusText: ${resposta.statusText}`);
	}
	*/
	//Realizando DELETE
	/*
	const id = 1;
	const resposta = await fetch(`${uri}/posts/${id}`, {method: "delete"});
	if (resposta.ok) {
		console.log("DELETE efetuado com sucesso!");
	} else {
		console.log(`Status: ${resposta.status}`);
		console.log(`StatusText: ${resposta.statusText}`);
	}
	*/
	//Realizando POST
	/*
	const id = 1;
	const postagem: PostDTO = {
		userId: 1,
		title: "Novo Post",
		body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ut dignissim libero, non volutpat nibh. Donec a massa sagittis, fringilla neque a, eleifend velit. Sed id pulvinar felis. Donec vel iaculis leo. Cras ac nulla vel tellus tristique auctor at at dolor. Vivamus fringilla metus a tellus vestibulum, a rhoncus massa blandit. Quisque vitae ipsum vel lacus ultricies luctus Aliquam fringilla enim eu ipsum pellentesque, at ornare dolor iaculis. Mauris luctus urna fermentum semper pulvinar. Proin consequat justo rhoncus massa vehicula, sed placerat erat rutrum. Phasellus id ligula suscipit, interdum lorem nec, pulvinar ex. Nulla et dui lorem. Ut faucibus, mi nec fringilla lacinia, ante velit dignissim tortor, laoreet viverra nulla ante vitae urna. Maecenas fringilla metus et quam faucibus vehicula. Mauris cursus metus id augue dapibus, vitae eleifend augue elementum.	Vivamus vitae nisl convallis, interdum erat et, rhoncus velit. Nullam dolor ipsum, lobortis at nunc at, porta varius erat. Donec aliquet luctus orci quis posuere. Etiam lobortis ante diam, non gravida risus accumsan in. Fusce eu ipsum ut ipsum consequat tempor tristique a nulla. Ut maximus tellus non varius imperdiet. Mauris sed quam vulputate velit lacinia venenatis. Nulla quis tellus diam. Phasellus feugiat quam sit amet ex blandit, id mollis erat efficitur. Duis nec enim sem. Donec iaculis posuere velit. Mauris at tincidunt lorem, ac facilisis mauris.",
	};
	const resposta = await fetch(`${uri}/posts`, {
		method: "post",
		headers: {
			"Content-type": "application/json; charset=UTF-8",
		},
		body: JSON.stringify(postagem),
	});
	if (resposta.ok) {
		const dadosJSON = (await resposta.json()) as PostDTO;
		console.log(dadosJSON);
		const cabecalhos = resposta.headers.get("Location");
		console.log(cabecalhos)

	} else {
		console.log(`Status: ${resposta.status}`);
		console.log(`StatusText: ${resposta.statusText}`);
	}
	*/
	//Realizando PUT
	const id = "1";
	const postagem: PostDTO = {
		id: 1,
		userId: 1,
		title: "Novo Post",
		body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ut dignissim libero, non volutpat nibh. Donec a massa sagittis, fringilla neque a, eleifend velit. Sed id pulvinar felis. Donec vel iaculis leo. Cras ac nulla vel tellus tristique auctor at at dolor. Vivamus fringilla metus a tellus vestibulum, a rhoncus massa blandit. Quisque vitae ipsum vel lacus ultricies luctus Aliquam fringilla enim eu ipsum pellentesque, at ornare dolor iaculis. Mauris luctus urna fermentum semper pulvinar. Proin consequat justo rhoncus massa vehicula, sed placerat erat rutrum. Phasellus id ligula suscipit, interdum lorem nec, pulvinar ex. Nulla et dui lorem. Ut faucibus, mi nec fringilla lacinia, ante velit dignissim tortor, laoreet viverra nulla ante vitae urna. Maecenas fringilla metus et quam faucibus vehicula. Mauris cursus metus id augue dapibus, vitae eleifend augue elementum.	Vivamus vitae nisl convallis, interdum erat et, rhoncus velit. Nullam dolor ipsum, lobortis at nunc at, porta varius erat. Donec aliquet luctus orci quis posuere. Etiam lobortis ante diam, non gravida risus accumsan in. Fusce eu ipsum ut ipsum consequat tempor tristique a nulla. Ut maximus tellus non varius imperdiet. Mauris sed quam vulputate velit lacinia venenatis. Nulla quis tellus diam. Phasellus feugiat quam sit amet ex blandit, id mollis erat efficitur. Duis nec enim sem. Donec iaculis posuere velit. Mauris at tincidunt lorem, ac facilisis mauris.",
	};
	const resposta = await fetch(`${uri}/posts/${id}`, {
		method: "put",
		headers: {
			"Content-type": "application/json; charset=UTF-8",
		},
		body: JSON.stringify(postagem),
	});
	if (resposta.ok) {
		const dadosJSON = (await resposta.json()) as PostDTO;
		console.log(dadosJSON);
	} else {
		console.log(`Status: ${resposta.status}`);
		console.log(`StatusText: ${resposta.statusText}`);
	}
} catch (error) {
	console.log("Falha de acesso ao web services: /n", error);
}
