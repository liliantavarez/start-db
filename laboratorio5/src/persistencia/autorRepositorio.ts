import {Autor} from "../entidades/autor.js";
import {AutorModel} from "./autorModel.js";

export async function criar(autor: Autor): Promise<Autor> {
	return AutorModel.create(autor); //retorna uma Promise
}

export async function buscar(): Promise<Autor[]> {
	let consulta = AutorModel.find();
	return consulta.exec(); //retorna uma Promise
}

export async function buscarUltimoNome(ultimo_nome: string): Promise<Autor[]> {
	let consulta = AutorModel.find({ultimo_nome});
	return consulta.exec(); //retorna uma Promise
}

//solução do professor
export async function buscarPorUltimoNome(nome: string): Promise<Autor[]> {
	let consulta = AutorModel.where("ultimo_nome", nome);
	return consulta.exec();
}

export async function buscarPrimeiroNome(primeiro_nome: string): Promise<Autor[]> {
	let consulta = AutorModel.find({primeiro_nome});
	return consulta.exec(); //retorna uma Promise
}

//solução do professor
export async function buscarPorPrimeiroNome(nome: string): Promise<Autor[]> {
	let consulta = AutorModel.where("primeiro_nome", nome);
	return consulta.exec();
}
