import {Livro} from "../entidades/livro.js";
import {AutorModel} from "./autorModel.js";
import {LivroModel} from "./livroModel.js";

export async function novoLivro(livro: Livro): Promise<Livro> {
	return LivroModel.create(livro);
}

//Solução do professor
export async function criar(livro: Livro): Promise<Livro> {
	return LivroModel.create(livro);
}

export async function buscarTodos(): Promise<Livro[]> {
	const busca = LivroModel.find({});
	return busca.exec();
}

//Solução do professor
export async function buscar() {
	return LivroModel.find()
		.populate({path: "autores", model: AutorModel})
		.exec();
}

export async function livroId(id: number): Promise<Livro[]> {
	const busca = LivroModel.find({_id: id}).populate({
		path: "autores",
		model: AutorModel,
	});
	return busca.exec();
}

//Solucao do professor
export async function buscaPorIsbn(isbn: string): Promise<Livro | null> {
	return LivroModel.findOne({isbn})
		.populate({path: "autores", model: AutorModel})
		.exec();
}
