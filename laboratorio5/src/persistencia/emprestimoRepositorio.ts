import {Emprestimo} from "../entidades/emprestimo.js";
import {EmprestimoModel} from "./emprestimoModel.js";

export async function criar(emprestimo: Emprestimo): Promise<Emprestimo> {
	return EmprestimoModel.create(emprestimo);
}

export async function buscaEmprestimos(): Promise<Emprestimo[]> {
	const busca = EmprestimoModel.find({});
	return busca.exec();
}

export async function alteraEmprestimo(id: number, emprestimo: Emprestimo) {
	const atualiza = EmprestimoModel.findByIdAndUpdate(id, emprestimo);
	return atualiza;
}

export async function verificarEmprestimoEmAberto(
	isbn: string,
): Promise<boolean> {
	const livro = await EmprestimoModel.find({isbn});
	if (livro.length === 0) {
		return false;
	}
	return true;
}
