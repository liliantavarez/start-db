import {Emprestimo} from "../entidades/emprestimo.js";
import {Livro} from "../entidades/livro.js";
import * as EmprestimoRepositorio from "../persistencia/emprestimoRepositorio.js";
import * as LivroRepositorio from "../persistencia/livroRepositorio.js";

function addDias() {
	var dataEntrega = new Date();
	dataEntrega.setDate(dataEntrega.getDate() + 7);
	return dataEntrega;
}

export async function consultarLivro(): Promise<Livro[]> {
	return LivroRepositorio.buscarTodos();
}

export async function emprestarLivro(isbn: string) {
	//isbn é de um livro na biblioteca?
	const livro = await LivroRepositorio.buscaPorIsbn(isbn);
	if (livro === null) {
		throw new Error("Livro inexistente");
	}
	//o livro esta disponível?
	const emprestado = await EmprestimoRepositorio.verificarEmprestimoEmAberto(
		isbn,
	);
	if (emprestado) {
		throw new Error(`Livro já emprestado!`);
	}
	//criar o empréstimo
	const emprestimo: Emprestimo = {
		livro,
		dataRetirada: new Date(),
		dataEntrega: addDias(),
	};
	//muda status do livro

	return EmprestimoRepositorio.criar(emprestimo);
}

export async function devolverLivro(isbn: string) {}
