import {connect, disconnect} from "mongoose";
import * as AutorRepositorio from "./persistencia/autorRepositorio.js";
import {LivroModel} from "./persistencia/livroModel.js";
import * as livroRepositorio from "./persistencia/livroRepositorio.js";
import * as emprestimoRepositorio from "./persistencia/emprestimoRepositorio.js";
import {EmprestimoModel} from "./persistencia/emprestimoModel.js";
import * as biblioteca from "./negocio/biblioteca.js";

try {
	const URL = process.env.MONGO_URL || "mongodb://localhost:27017/";
	await connect(URL);
	console.log("Conectado ao MongoDb Atlas");

	/*
	console.log("Adicionando autores...");
	let a1 = await AutorRepositorio.criar({
		primeiro_nome: "John",
		ultimo_nome: "Doe",
	});
	console.log(`Autor inserido: ${a1}`);
	let a2 = await AutorRepositorio.criar({
		primeiro_nome: "Mary",
		ultimo_nome: "Doe",
	});
	console.log(`Autor inserido: ${a2}`);
	let a3 = await AutorRepositorio.criar({
		primeiro_nome: "Bob",
		ultimo_nome: "Jhon",
	});
	console.log(`Autor inserido: ${a3}`);
	*/

	/*
	console.log("Buscando autores...");
	let autores = await AutorRepositorio.buscar();
	autores.forEach((autor) => console.log(autor));
	*/

	/*
	console.log("Buscando autores por ultimo nome");
	let autores = await AutorRepositorio.buscarPorUltimoNome("Doe");
	autores.forEach((autor) => console.log(autor));

	console.log("Adicionado livros...");
	let livro1 = await livroRepositorio.criar({
		titulo: "Mongoose com Node.js",
		autores: [autores[0], autores[1]],
		isbn: "12345",
	});
	console.log(`Livro inserido ${livro1}`);
	*/

	/*
	console.log("Adicionado livros com um novo autor..");
	//Vai dar erro!!
	let livro2 = await livroRepositorio.criar({
		titulo: "React Js",
		autores: [{primeiro_nome:"Julio", ultimo_nome:"Machado"}],
	});
	console.log(`Livro inserido ${livro2}`);
	*/

	/*
	console.log("Buscando livros.. ");
	let livros = await livroRepositorio.buscar();
	console.log(livros);
	*/
	/*
	console.log("Buscar livro por isbn");
	let livro = await livroRepositorio.buscaPorIsbn("12345");
	console.log(livro);
	*/

	console.log("Realizar emprestimo");
	let emprestimo = await biblioteca.emprestarLivro("12345");
	console.log(emprestimo);
	
} catch (error) {
	console.log(`${error}`);
} finally {
	await disconnect();
	console.log("Desconectado do MongoDb Atlas");
}
console.log("Fim do programa!");
