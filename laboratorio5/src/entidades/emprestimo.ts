import {Livro} from "./livro.js";

export interface Emprestimo {
	livro: Livro;
	dataRetirada: Date;
	dataEntrega?: Date;
}
