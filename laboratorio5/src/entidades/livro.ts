import {Autor} from "./autor.js";

export interface Livro {
	titulo: string;
	autores: Autor[];
	isbn: string;
}
