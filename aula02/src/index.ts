function numerosPares(numeros: number[]): number[] {
	const numerosPares: number[] = [];
	numeros.forEach((numero) => {
		if (numero % 2 === 0) {
			numerosPares.push(numero);
		}
	});
	return numerosPares;
}

function numerosParesWhile(numeros: number[]): number[] {
	let index: number = 0;
	let numerosPares: number[] = [];
	while (index <= numeros.length) {
		if (numeros[index] % 2 === 0) {
			numerosPares.push(numeros[index]);
		}
		index++;
	}
	return numerosPares;
}

const arrNumerosPares: number[] = numerosPares([6, 3, 13, 28, 7, 12, 5]);
const arrNumerosParesWhile: number[] = numerosParesWhile([2, 3, 1, 8, 7, 4]);

console.log(arrNumerosPares);
console.log(arrNumerosParesWhile);

//O programa entrar em um loop infinito já que em nenhum momento a variável i antedera a condição de ser igual a 10

function min(numero1: number, numero2: number): number {
	if (numero1 < numero2) {
		return numero1;
	} else {
		return numero2;
	}
}

const numMenor: number = min(4, 3);
const numMenor2: number = min(2, 5);
const numMenor3: number = min(9, 7);

console.log(numMenor, numMenor2, numMenor3);

function pow(x: number, y: number): number {
	let resultado: number = x;
	if (y === 0) {
		return 1;
	} else {
		for (let index = y; index > 1; index--) {
			resultado *= x;
		}
	}
	return resultado;
}

console.log(pow(3, 7));
console.log(pow(2, 3));
console.log(pow(5, 1));
console.log(pow(7, 0));

function powRec(x: number, y: number): number {
	if (y === 0) {
		return 1;
	} else {
		return x * powRec(x, y - 1);
	}
}

console.log(powRec(3, 7));
console.log(powRec(2, 3));
console.log(powRec(5, 1));
console.log(powRec(7, 0));

function toMaiusculaPrimeira(s: string): string {
	return s[0].toUpperCase() + s.substring(1);
}

console.log(toMaiusculaPrimeira("hello world!"));
console.log(toMaiusculaPrimeira("usando typescript!!"));

function getMax(arr: number[]): number {
	let maior: number = 0;
	for (let index = 0; index < arr.length; index++) {
		if (arr[index] >= maior) {
			maior = arr[index];
		}
	}
	return maior;
}

console.log(getMax([10, 3, 13, 38, 77]));
console.log(getMax([0, 63, 12, 18, 4]));
console.log(getMax([10, 23, 93, 38, 50]));

function frequencia(arr: number[]): Map<number, number> {
	const tabela = new Map<number, number>();
	arr.forEach((numero) => {
		if (!tabela.has(numero)) {
			tabela.set(numero, 1);
		} else {
			const contagemAtual = tabela.get(numero);
			tabela.set(numero, contagemAtual! + 1);
		}
	});
	return tabela;
}
console.log(frequencia([1, 2, 3, 5, 3, 1, 5, 2]));

function frequenciaV2(arr: number[]): Map<number, number> {
	return arr.reduce(
		(tabela, numero) => tabela.set(numero, (tabela.get(numero) || 0) + 1),
		new Map<number, number>(),
	);
}
console.log(frequenciaV2([1, 2, 3, 3, 2, 4, 2, 1]));
