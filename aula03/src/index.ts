//Listas
let pilha: number[] = [];
pilha.push(1);
pilha.push(2);
pilha.push(3);
console.log(pilha.length);
console.log(pilha);
pilha.pop(); //remove do fim
console.log(pilha.length);
console.log(pilha);

//Filhas
let pilhaFila: number[] = [];
pilhaFila.push(1);
pilhaFila.push(2);
pilhaFila.push(3);
console.log(pilhaFila.length);
console.log(pilhaFila);
pilhaFila.shift(); //remove do inicio
console.log(pilhaFila.length);
console.log(pilhaFila);

//Filas de extremidade dupla
let deque: number[] = [1, 2, 3];
deque.push(4); //inseri no fim
console.log(deque);
deque.pop(); // remove do fim
console.log(deque);
deque.unshift(0); //inseri no inicio
console.log(deque);
deque.shift(); //remove no inicio
console.log(deque);

let mapa = new Map<string, string>();
mapa.set("RS", "Rio Grande do Sul");
mapa.set("RC", "Santa Catarina");
mapa.set("PR", "Paraná");
console.log(mapa.has("RJ"));
console.log(mapa.get("RJ"));
for (const estado of mapa.keys()) {
	console.log(estado);
}

//Conjuntos
let conjunto = new Set<number>();
conjunto.add(1);
conjunto.add(1);
conjunto.add(1);
conjunto.add(2);
conjunto.add(3);
conjunto.add(4);

console.log(conjunto.size);
