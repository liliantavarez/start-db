import {Router} from "express";
import {getAlo, getAloComNome, postAloJson} from "../controllers/alo.controller.js";

const router = Router();
const path = "/alo";

router.get(path, getAlo);
router.get(`${path}:nome`, getAloComNome);

router.post(path, postAloJson);

export {router, path};
