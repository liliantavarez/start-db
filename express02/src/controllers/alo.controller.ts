import {Request, Response} from "express";

export function getAlo(req: Request, res: Response) {
	res.send("Alo Mundo");
}

export function getAloComNome(req: Request, res: Response) {
	const nome = req.params.nome;
	res.send(`Alo ${nome}`);
}

export function postAloJson(req: Request, res: Response) {
	const {nome} = req.body;
	if (nome) {
		res.send(`Alo ${nome}`);
	} else {
		res.status(400).send("Nome não informado!");
	}
}
