import app from "./config/app.js";

app.listen(app.get("port"), () => {
	console.log(`Servidor conectado na porta ${app.get("port")}`);
	console.log(`Express no modo: ${app.get("env")}`);
});
