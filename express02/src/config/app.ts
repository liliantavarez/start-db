//Configurações do Express
import express from "express";
import cors from "cors";
import errorHandler from "errorhandler";
import {createLogger, transports} from "winston";
import {router as aloRouter} from "../routes/alo.routes.js";

const app = express();
const portaPadrao = 1313;

app.set("port", process.env.PORT || portaPadrao);
app.use(cors());
app.use(express.json());

if (process.env.NODE_ENV != "production") {
	const logger = createLogger({
		transports: [new transports.Console()],
	});
	app.use(errorHandler());
}

app.use(`/api/v${process.env.API_VERSION} || '0.0.1'`, aloRouter);

export default app;
