export function area(r: number) {
	return Math.PI * r ** 2;
}

export function circunferencia(r: number) {
	return 2 * Math.PI * r;
}
