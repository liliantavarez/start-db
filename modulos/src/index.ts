import {area, circunferencia} from "./circulo_funcoes.js";
import Circulo from "./circulo_objeto.js";
import * as fs from "node:fs/promises";

console.log(area(3));
console.log(circunferencia(8));

let c = new Circulo(6);

console.log(c.area());
console.log(c.circunferencia());

/*
const json = JSON.stringify(c);
fs.writeFile("dados.json", json)
	.then(() => console.log("Arquivo escrito com sucesso"))
	.catch((err) => {
			console.log(`Falha de escrita: ${err}`);
        });
        */

try {
	const json = await fs.readFile("dados.json", "utf8");
	console.log(json);
} catch (error) {
	console.log(`Falha de leitura: ${error}`);
}
