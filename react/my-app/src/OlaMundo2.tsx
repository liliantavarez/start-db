
interface OlaMundo2Porps {
    nome: string,
    cor?:string 
}

export function OlaMundo2(props: OlaMundo2Porps) {
    const corTexto = props.cor || 'red'
    return (
        <h1 style={{color:corTexto}}>Olá {props.nome}!</h1>
    )
}