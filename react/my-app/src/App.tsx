import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { OlaMundo } from "./OlaMundo";
import { OlaMundo2 } from "./OlaMundo2";
// import { MeuBotao } from "./MeuBotao";

function App() {
  return (
    <div className="App">
      <OlaMundo />
      <OlaMundo2 cor="blue" nome="Lilian" />
      {/* <MeuBotao /> */}
    </div>
  );
}

export default App;
