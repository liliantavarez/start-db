// Funções anonimas
(function (x: number, y: number) {
	return x + y;
})(5, 4);

// Função que retorna outra função
function somar(x: number): (y: number) => number {
	return function (y: number) {
		return x + y;
	};
}

let somarDois = somar(2);
console.log(somarDois(6));

//Arrow Functions || Expressões lambidas
function somarArrow(x: number): (y: number) => number {
	return (y: number) => x + y;
}

let somarDoisArrow = somarArrow(6);
console.log(somarDoisArrow(7));

const somarXY = (x: number, y: number) => x + y;
console.log(somarXY(12, 3));

const somarV2 = (x: number, y: number) => {
	return x + y;
};
console.log(somarV2(5, 9));

//Matados de busca com array
let array = [4, 5, 6, 7, 8, 9, 10];

let resultado = array.find((n) => n + 1 == 10);
console.log(resultado);

let resultado2 = array.findIndex((n) => n + 1 == 10);
console.log(resultado2);

const resultadoFilter = array.filter((number) => number > 7);
console.log(resultadoFilter);

const impar = (n: number) => n % 2 != 0;
const resultadoFilter2 = array.filter(impar);
console.log(resultadoFilter2);

let resultadoMap = array.map((n) => n * 2);
console.log(resultadoMap);

let resultadoMap2 = array.map((n) => n - 10 / 100);
console.log(resultadoMap2);

let resultadoFilterMap = array.filter((n) => n % 2 != 1).map((n) => n * 2);
console.log(resultadoFilterMap);

let resultadoReduce = array.reduce(
	(valorAnterio, valorAtual) => valorAnterio + valorAtual,
	0,
);
console.log(resultadoReduce / array.length);
