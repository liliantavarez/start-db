class Circulo {
	constructor(
		private _raio: number,
		private _pontox: number,
		private _pontoy: number,
	) {}

	public set raio(r: number) {
		this._raio = r;
	}
	public get raio(): number {
		return this._raio;
	}

	public set pontox(x: number) {
		this._pontox = x;
	}
	public get pontox(): number {
		return this._pontox;
	}

	public set pontoy(y: number) {
		this._pontoy = y;
	}
	public get pontoy(): number {
		return this._pontoy;
	}

	calcularArea() {
		return Math.PI * this._raio ** 2;
	}

	calcularCircuferencia() {
		return 2 * Math.PI * this._raio;
	}

	static calcularEquacaoGeral(r: number, x: number, y: number) {
		const a = -2 * x;
		const b = -2 * y;
		const c = x ** 2 + y ** 2 - r ** 2;
		let eq = "x² + y²";

		if (a > 0) {
			eq += `+ ${a}`;
		} else if (a < 0) {
			eq += ` ${a}x`;
		}

		if (b > 0) {
			eq += `+ ${b}y `;
		} else if (b < 0) {
			eq += ` ${b}y `;
		}

		if (c > 0) {
			eq += `+ ${c}`;
		} else if (c < 0) {
			eq += ` ${c}`;
		}
		eq += " = 0";
		return eq;
	}
}

const c = new Circulo(3, 4, 5);
console.log(c.calcularArea());
console.log(Circulo.calcularEquacaoGeral(2, 3, 1));
