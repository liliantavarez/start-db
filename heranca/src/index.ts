class Produto {
	constructor(private _nome: string, private _preco: number) {}

	public get nome(): string {
		return this._nome;
	}

	public get preco(): number {
		return this._preco;
	}

	toString() {
		return `\nProduto: ${this.nome} \nValor: ${this.preco}`;
	}
}

class ProdutoPerecivel extends Produto {
	constructor(nome: string, preco: number, private _dataValidade: Date) {
		super(nome, preco);
	}

	toString(): string {
		return super.toString() + `\nData de Validade: ${this._dataValidade.toLocaleDateString()}`;
	}
}

const p = new Produto("Caderno", 5.26);
const pp = new ProdutoPerecivel("Arroz", 9.35, new Date(2029, 10, 15));
const produtos: Produto[] = [];
produtos.push(p);
produtos.push(pp);

produtos.forEach((produto) => {
	console.log(produto.toString());
});
