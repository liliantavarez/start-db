import {Cofrinho, Moeda} from "./classes/entidades.js";
import * as persistencia from "./classes/persistencia.js";

const cofre = new Cofrinho();
cofre.adicionar(new Moeda(1, "um real"));
cofre.adicionar(new Moeda(0.5, "cinquenta centavos"));
cofre.adicionar(new Moeda(0.1, "dez centavos"));
cofre.adicionar(new Moeda(0.25, "vinte e cinco centavos"));

try {
	// await persistencia.salvarCofrinho(cofre, "Cofrinho.json");
	// console.log("Cofre armazenado com sucesso!")
	const cofre2 = await persistencia.lerCofrinho("Cofrinho.json");
	console.log(cofre2);
    console.log(cofre.calcularTotal())
} catch (error) {
	console.log("Ops, algo deu errado: ", error);
}
console.log("Fim do programa!");
