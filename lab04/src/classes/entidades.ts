class Moeda {
	constructor(private _valor: number, private _nome: string) {}

	public get valor(): number {
		return this._valor;
	}

	public get nome(): string {
		return this._nome;
	}
}

class Cofrinho {
	private _moedas: Moeda[] = [];

	adicionar(m: Moeda) {
		this._moedas.push(m);
	}
	calcularTotal() {
		return this._moedas.reduce((total, m) => total + m.valor, 0);
	}
}

export {Moeda, Cofrinho};
