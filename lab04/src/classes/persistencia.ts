import * as fs from "node:fs/promises";
import {Moeda, Cofrinho} from "./entidades.js";

async function salvarCofrinho(cofrinho: Cofrinho, nomeArquivo: string) {
	const json = JSON.stringify(cofrinho);
	return fs.writeFile(nomeArquivo, json);
}
//

async function lerCofrinho(nomeArquivo: string) {
	const json = await fs.readFile(nomeArquivo, "utf8");
	const obj = JSON.parse(json);
	const cofrinho = new Cofrinho();
	obj._moedas.forEach((m: any) => {
		cofrinho.adicionar(new Moeda(m._valor, m._nome));
	});

	return cofrinho;
}

export {salvarCofrinho, lerCofrinho};
