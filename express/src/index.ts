import express from "express";

const app = express();
const PORT: number = 1313;

app.use(express.json());
//GET
app.get("/", (req, res) => {
	res.send("Olá Mundo!");
});

app.get("/:nome", (req, res) => {
	console.log(req.body);
	res.send(`Olá Mundo! Seja bem vinde ${req.params.nome}`);
});

app.post("/", (req, res) => {
	const {nome} = req.body;
	if (nome) {
		res.send(`Olá ${nome}`)
	} else {
		res.status(400).send(`Nome não informado!`)
	}
});

app.listen(PORT, () => {
	console.log(`Servidor conectado na porta ${PORT}`);
	console.log(`Express no modo: ${app.get("env")}`);
});
