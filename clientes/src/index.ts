abstract class Cliente {
	constructor(private _nome: string) {}

	public get nome(): string {
		return this._nome;
	}

	abstract get mensalidade(): number;
}

class ClienteFisico extends Cliente {
	constructor(nome: string, private _idade: number, private _salario: number) {
		super(nome);
	}

	public get idade() {
		return this._idade;
	}

	public set idade(idade: number) {
		this._idade = idade;
	}

	public get salario() {
		return this._salario;
	}

	public set salario(salario: number) {
		this._salario = salario;
	}

	get mensalidade(): number {
		if (this._idade <= 60) {
			return (10 * this._salario) / 100;
		} else {
			return (15 * this._salario) / 100;
		}
	}
}

class ClienteJuridico extends Cliente {
	constructor(nome: string, private _mensalidade: number) {
		super(nome);
	}

	public set mensalidade(m: number) {
		this._mensalidade = m;
	}

	get mensalidade(): number {
		return this._mensalidade;
	}
}

const clientes: Cliente[] = [];

const cf = new ClienteFisico("Lilian", 28, 1100);
const cf2 = new ClienteFisico("Marcela", 78, 1100);
const cj = new ClienteJuridico("Rita", 123);

clientes.push(cf);
clientes.push(cf2);
clientes.push(cj);

clientes.forEach((cliente) => {
	console.log(
		`Nome: ${cliente.nome}\nValor da Mensalidade: ${cliente.mensalidade}\n`,
	);
});
