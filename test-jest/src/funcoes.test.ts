import {stringBinParaNumber, somar, somarAsync} from "./funcoes.js";

describe("somar", () => {
	test("deve retornar 3 para 1 + 2", () => {
		expect(somar(1, 2)).toBe(3);
	});
	test.each([
		[0, 0, 0],
		[1, 0, 1],
		[-1, -1, 0],
		[0, -1, 1],
	])("deve retornar %i para %i + %i", (r, x, y) => {
		expect(somar(x, y)).toBe(r);
	});
});

describe("somarAsync", () => {
	test("deve retornar 2 para 1 + 1", async () => {
		const r = await somarAsync(1, 1);
		expect(r).toBe(2);
	});
	test("deve retornar 0 para 0 + 0", async () => {
		expect(somarAsync(0, 0)).resolves.toBe(0);
	});
});

describe("stringBinParaNumber", () => {
	test('deve gerar exceção Error para "abc"', () => {
		expect(() => stringBinParaNumber("abc")).toThrow();
		expect(() => stringBinParaNumber("abc")).toThrow(Error);
		expect(() => stringBinParaNumber("abc")).toThrow(
			"Número binário inválido.",
		);
	});
});
