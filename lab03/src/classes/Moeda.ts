export default class Moeda {
	constructor(private _valor: number, private _nome: string) {}

	public get valor(): number {
		return this._valor;
	}

	public get nome(): string {
		return this._nome;
	}
}
