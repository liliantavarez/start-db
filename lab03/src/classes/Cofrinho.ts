import Moeda from "./Moeda.js";
export default class Cofrinho {
	private _moedas: Moeda[] = [];

	adicionar(moeda: Moeda) {
		this._moedas.push(moeda);
	}

	calcularTotal() {
		let total = 0;
		this._moedas.forEach((m) => {
			total += m.valor;
		});
		return total;
	}
}
