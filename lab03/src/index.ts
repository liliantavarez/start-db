import Cofrinho from "./classes/Cofrinho.js";
import Moeda from "./classes/Moeda.js";

const moeda1 = new Moeda(1, "um real");
const moeda5 = new Moeda(0.5, "cinquenta centavos");
const moeda10 = new Moeda(0.1, "dez centavos");
const moeda25 = new Moeda(0.25, "vinte e cinco centavos");
const c = new Cofrinho();
c.adicionar(moeda10);
c.adicionar(moeda5);
c.adicionar(moeda1);
c.adicionar(moeda25);

console.log(`Valor dentro do cofrinho: R$ ${c.calcularTotal().toFixed(2)}`);

const json = JSON.stringify(c);
console.log(json);
