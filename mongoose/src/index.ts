import {connect, disconnect} from "mongoose";
import {Pessoa, PessoaModel} from "./schema/pessoa.js";

const URL =
	"mongodb+srv://admin:i5C9KQrC9iHPY425@startdb.fgjxak1.mongodb.net/?retryWrites=true&w=majority";

try {
	console.log("Conectando com o MongoDB...");
	await connect(URL);
	/*
	console.log("Inserindo....");
	const pessoa: Pessoa = {
		nome: "Carla Rodrigues",
		idade: 14,
		email: "mariadasilva@gmail.com",
	};

	const documentoInserido = await PessoaModel.create(pessoa);
	console.log(documentoInserido);
	*/
	/*
	console.log("Consulta simples");
	const pessoas = await PessoaModel.find().exec();
	console.log(pessoas);
	*/
	/*
	console.log("Consulta com filtros..");
	const numero = await PessoaModel.where("idade")//Define o parâmetro que sera usado como filtro
		.lte(18) //Menor ou igual 
		.countDocuments() //Conta quantas ocorrências correspondem ao filtro
		.exec(); //Executa a query
	console.log(numero);
	*/

	/*
	console.log("Alterando..");
	const documento = await PessoaModel.findById("632b6b998060c256bf940c2c").exec();
	if(documento!=null){
		documento.email = 'carla@gmail.com'
		const documentoAtualizado = await documento.save()
		console.log(documentoAtualizado.nome)
	}
	*/
	
	console.log("Removendo.. ")
	const documento = await PessoaModel.findById('632b6b998060c256bf940c2c').exec()
	if(documento!=null){
		const documentoRemovido = await documento.remove();
		console.log(documentoRemovido);
	}
} catch (error) {
	console.log("Erro ao conectar com MongoDB...");
	console.log(error);
} finally {
	await disconnect();
}
console.log("Fim do programa");
