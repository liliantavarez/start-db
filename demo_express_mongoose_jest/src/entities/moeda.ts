export interface Moeda {
    codigo: string;
    nome: string;
    cotacao: number; //taxa de BRL para a moeda específica
}
