export interface ConversaoDTO {
    moedaOrigem: string;
    moedaDestino: string;
    valorConvesao: number;
}