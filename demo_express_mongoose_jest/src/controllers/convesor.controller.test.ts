import { jest } from '@jest/globals';
import { getMockReq, getMockRes } from '@jest-mock/express';
import { type } from 'os';
import { ConversaoDTO } from './conversaodto.js';
import { ConversorController } from './conversor.controller.js';
import { MoedaRepository } from '../repositories/moedarepository.js';

describe('ConversorController', () => {
    //Valores e resultado esperado
    const codigoMoeda = 'ARS';
    const resultadoEsperado: ConversaoDTO = {
        moedaOrigem: 'BRL',
        moedaDestino: 'ARS',
        valorConvesao: 27.248
    };
    const valorParaConversao = 1;
    const valorConvetidoEsperado = 27.248;

    beforeEach(() => {
        jest.resetModules();
    });

    test(`deve converter de ${valorParaConversao} BRL para ${valorConvetidoEsperado} ${codigoMoeda}`, async () => {
        //Dublê (mock) para o conversor
        const mockConverterRealPara = jest.fn(async (c: string, v: number) => valorConvetidoEsperado);
        jest.unstable_mockModule('../usecases/conversor.js', () => {
            return {
                Conversor: jest.fn((r: MoedaRepository) => {
                    return {
                        converterRealPara: mockConverterRealPara
                    };
                })
            };
        });
        const { Conversor } = await import('../usecases/conversor.js');
        //ConversorController a ser testado com mock
        const conversorControllerComMock = new ConversorController(new Conversor({} as MoedaRepository));
        const req = getMockReq({
            query: {
                moeda: codigoMoeda,
                valor: valorParaConversao.toString()
            }
        });
        const { res, next } = getMockRes();
        await conversorControllerComMock.tratador(req, res, next);
        
        expect(mockConverterRealPara).toBeCalledTimes(1);
        expect(mockConverterRealPara).toBeCalledWith(codigoMoeda, valorParaConversao);
        expect(res.json).toHaveBeenCalledWith(
            expect.objectContaining(resultadoEsperado),
        );
    });
});