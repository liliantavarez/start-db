import { Conversor } from '../usecases/conversor.js';
import { Request, Response, NextFunction } from 'express';
import { ConversaoDTO } from './conversaodto.js';

export class ConversorController {
    constructor(private conversor: Conversor){
    }
    //GET /conversor?moeda=:codigo&valor=:valor
    async tratador(req: Request, res: Response, next: NextFunction) {
        try {
            const queryCodigoMoeda = req.query.moeda;
            const queryValor = req.query.valor;
            if (queryCodigoMoeda && queryValor) {
                const codigoMoeda = queryCodigoMoeda.toString();
                const valor = parseFloat(queryValor.toString());
                const resultado = await this.conversor.converterRealPara(codigoMoeda, valor);
                const resultadoDto: ConversaoDTO = {
                    moedaOrigem: 'BRL',
                    moedaDestino: codigoMoeda,
                    valorConvesao: resultado
                };
                res.json(resultadoDto);
            } else {
                res.status(400).send('Parâmetros inválidos.');
            }
        } catch (error) {
            next(error);
        }
    }
}