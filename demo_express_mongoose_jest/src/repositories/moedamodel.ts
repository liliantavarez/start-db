import {Moeda} from '../entities/moeda.js';
import {model, Schema} from 'mongoose';

const MoedaSchema = new Schema<Moeda>({
    codigo: {type: String, required: true, minlength: 3, maxlength: 3},
    nome: {type: String, required: true, maxlength: 30},
    cotacao: {type: Number, required: true, min: 0}
});

export const MoedaModel = model<Moeda>('Moeda', MoedaSchema, 'moedas');
