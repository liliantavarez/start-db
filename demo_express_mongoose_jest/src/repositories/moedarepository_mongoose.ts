import {Moeda} from '../entities/moeda.js';
import {MoedaRepository} from './moedarepository.js';
import {MoedaModel} from './moedamodel.js';

export class MoedaRepositoryMongoose implements MoedaRepository {
    buscarPorCodigo(codigo: string): Promise<Moeda | null> {
        const consulta = MoedaModel
            .findOne()
            .where('codigo', codigo);
        return consulta.exec();
    }
}
