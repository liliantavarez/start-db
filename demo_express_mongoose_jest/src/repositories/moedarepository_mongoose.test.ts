import {Moeda} from '../entities/moeda.js';
import {MoedaRepository} from './moedarepository.js';
import {MoedaRepositoryMongoose} from './moedarepository_mongoose.js';
import baseDeDados from '../helper/tests/mongodb_helper.js';

describe('MoedaRepositoryMongoose', () => {
    beforeAll(async () => {
        await baseDeDados.abrir();
    });
    afterAll(async () => {
        await baseDeDados.fechar();
    });
    beforeEach(async () => {
        await baseDeDados.inicializarDados();
    });
    afterEach(async () => {
        await baseDeDados.limpar();
    });
    describe('buscarPorCodigo()', () => {
        const repositorio: MoedaRepository = new MoedaRepositoryMongoose();
        test('deve retornar null para código de moeda inexistente', async () => {
            const codigo = 'ZZZ';
            const moeda = await repositorio.buscarPorCodigo(codigo);
            expect(moeda).toBeNull();
        });
        test('deve retornar moeda correta para código de moeda USD', async () => {
            const codigo = 'USD';
            const moeda = await repositorio.buscarPorCodigo(codigo);
            expect(moeda).toBeDefined();
            expect(moeda?.codigo).toBe('USD');
            expect(moeda?.nome).toBe('Dólar dos Estados Unidos');
            expect(moeda?.cotacao).toBe(0.1865881);
        });
    });
});