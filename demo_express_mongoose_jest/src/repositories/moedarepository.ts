import {Moeda} from '../entities/moeda.js';

export interface MoedaRepository {
    buscarPorCodigo(codigo: string): Promise<Moeda|null>;
}
