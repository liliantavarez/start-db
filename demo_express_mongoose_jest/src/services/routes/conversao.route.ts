import { Router } from 'express';
import { criarConversorController } from '../factories/conversor.factory.js';

const router = Router();
const basePath = 'conversao';
const conversorController = criarConversorController();
//Aqui é necessário realizar um bind explícito da instância do objeto para que
//a referência 'this' dentro de ConversorController não seja 'undefined'.
//Motivo: passar a referência para o método do objeto faz perder o binding com a instância.
router.get('', conversorController.tratador.bind(conversorController));
export {router, basePath};
