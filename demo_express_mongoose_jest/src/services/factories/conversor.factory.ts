import { ConversorController } from '../../controllers/conversor.controller.js';
import { Conversor } from '../../usecases/conversor.js';
import { MoedaRepositoryMongoose } from '../../repositories/moedarepository_mongoose.js';

export function criarConversorController() {
    const moedaRepositoryMongoose = new MoedaRepositoryMongoose();
    const conversor = new Conversor(moedaRepositoryMongoose);
    const conversorController = new ConversorController(conversor);
    return conversorController;
}
