import { Express } from 'express';
import * as RotaConversao from '../../services/routes/conversao.route.js';

export default function routes(app: Express) {
    const api = process.env.API_VERSION ? `/api/v${process.env.API_VERSION}`
                                        : '/api';
    //Rota de conversão
    console.log(`Rota ativa: ${api}/${RotaConversao.basePath}`);
    app.use(`${api}/${RotaConversao.basePath}`, RotaConversao.router);
}
