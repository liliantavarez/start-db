import { jest } from '@jest/globals';
import { Conversor } from './conversor.js';
import { Moeda } from '../entities/moeda.js';
import { MoedaRepository } from '../repositories/moedarepository.js';

class MoedaRepositoryStub implements MoedaRepository {
    async buscarPorCodigo(codigo: string): Promise<Moeda | null> {
        if (codigo === 'USD') {
            return {
                codigo: 'USD',
                nome: 'Dólar dos Estados Unidos',
                cotacao: 0.1865881
            };
        }
        return null;
    }
}

describe('Convesor', () => {
    //Valores e resultado esperado
    const codigoMoeda = 'USD';
    const moedaEsperada: Moeda = {
        codigo: 'USD',
        nome: 'Dólar dos Estados Unidos',
        cotacao: 0.1865881
    };
    const valorParaConversao = 2;
    const valorConvetidoEsperado = 0.3731762;

    describe('converterRealPara() com stub', () => {
        //Dublê (stub) para o repositório
        const moedaRepositorioStub = new MoedaRepositoryStub();
        //Conversor a ser testado com stub
        const conversorComStub = new Conversor(moedaRepositorioStub);
        test('deve converter de BRL para USD', async () => {
            const resultado = await conversorComStub.converterRealPara(codigoMoeda, valorParaConversao);
            expect(resultado).toBeCloseTo(valorConvetidoEsperado);
        });
        test('deve lançar exceção ao converter de BRL para ZZZ', async () => {
            try {
                const resultado = await conversorComStub.converterRealPara('ZZZ', valorParaConversao);
            } catch (error) {
                expect(error).toBeInstanceOf(Error);
            }
        });
    });

    describe('converterRealPara() com mock', () => {
        beforeEach(() => {
            jest.resetModules();
        });
        test('deve converter de BRL para USD', async () => {
            //Dublê (mock) para o repositório
            const mockBuscarPorCodigo = jest.fn(async (c: string) => moedaEsperada);
            jest.unstable_mockModule('../repositories/moedarepository_mongoose.js', () => {
                return {
                    MoedaRepositoryMongoose: jest.fn().mockImplementation(() => {
                        return {
                            buscarPorCodigo: mockBuscarPorCodigo
                        };
                    })
                };
            });
            const { MoedaRepositoryMongoose } = await import('../repositories/moedarepository_mongoose.js');
            //Conversor a ser testado com mock
            const conversorComMock = new Conversor(new MoedaRepositoryMongoose());
            const resultado = await conversorComMock.converterRealPara(codigoMoeda, valorParaConversao);
            expect(mockBuscarPorCodigo).toHaveBeenCalledTimes(1);
            expect(mockBuscarPorCodigo).toHaveBeenCalledWith(codigoMoeda);
        });
    });

});