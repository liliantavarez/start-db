import {Moeda} from '../entities/moeda.js';
import {MoedaRepository} from '../repositories/moedarepository.js';

export class Conversor {
    constructor(private repositorio: MoedaRepository){}
    async converterRealPara(codigoMoeda: string, valor: number) {
        const moeda = await this.repositorio.buscarPorCodigo(codigoMoeda);
        if (!moeda) {
            throw new Error('Código de moeda inexistente');
        }
        return valor * moeda.cotacao;
    }
}