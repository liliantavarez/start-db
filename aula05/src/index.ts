import Cliente from "./classes/Cliente.js";
import ClienteFisico from "./classes/ClienteFisico.js";
import ClienteJuridico from "./classes/ClienteJuridico.js";
const clientes: Cliente[] = [];

const cf = new ClienteFisico("Lilian", 28, 1100);
const cf2 = new ClienteFisico("Marcela", 78, 1100);
const cj = new ClienteJuridico("Rita", 123);

clientes.push(cf);
clientes.push(cf2);
clientes.push(cj);

clientes.forEach((cliente) => {
	console.log(
		`Nome: ${cliente.nome}\nValor da Mensalidade: ${cliente.mensalidade}\n`,
	);
});

console.log(clientes);
const clientesJSON = JSON.stringify(clientes);
console.log(clientesJSON);
console.log(JSON.parse(clientesJSON));
