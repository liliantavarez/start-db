import Cliente from "./Cliente.js";

export default class ClienteFisico extends Cliente {
	constructor(nome: string, private _idade: number, private _salario: number) {
		super(nome);
	}

	public get idade() {
		return this._idade;
	}

	public set idade(idade: number) {
		this._idade = idade;
	}

	public get salario() {
		return this._salario;
	}

	public set salario(salario: number) {
		this._salario = salario;
	}

	get mensalidade(): number {
		if (this._idade <= 60) {
			return (10 * this._salario) / 100;
		} else {
			return (15 * this._salario) / 100;
		}
	}
}
