import Cliente from "./Cliente.js";

export default class ClienteJuridico extends Cliente {
	constructor(nome: string, private _mensalidade: number) {
		super(nome);
	}

	public set mensalidade(m: number) {
		this._mensalidade = m;
	}

	get mensalidade(): number {
		return this._mensalidade;
	}
}
