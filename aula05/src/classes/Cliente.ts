export default abstract class Cliente {
	constructor(private _nome: string) {}

	public get nome(): string {
		return this._nome;
	}

	abstract get mensalidade(): number;
}

