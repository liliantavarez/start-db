import {Pessoa} from "./entidades.js";
import {MongoClient, ObjectId} from "mongodb";
import * as dotenv from "dotenv"; // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
// import express from 'express';
dotenv.config();

const url =
	"mongodb+srv://admin:i5C9KQrC9iHPY425@startdb.fgjxak1.mongodb.net/?retryWrites=true&w=majority";
const cliente = new MongoClient(url);
//demo_bd.pessoas

try {
	console.log("Conectando com MongoDB");
	await cliente.connect();
	console.log("Conectado com sucesso ao MongoDB");

	const bd = cliente.db("demo_bd");
	const colecao = bd.collection("pessoas");

	console.log("Consultando documentos");
	/*Sem Filtro
	const documentos = await colecao.find<Pessoa>({}).toArray();
	console.log(documentos);
	documentos.forEach(p => console.log(p.nome))
	*/
	/*
	const documentos = await colecao.find<Pessoa>({idade:{$lte:30}}).toArray();
	console.log(documentos);

	//Retorna a quantidade 
	const quantidade = await colecao.countDocuments({idade:{$lte:30}});
	console.log(quantidade);
    
	console.log("Inserindo documentos");
	const pessoa: Pessoa = {
		nome: "Cassio",
		idade: 32,
	};

	const resultado = await colecao.insertOne(pessoa);
	console.log(resultado.insertedId);
	*/

	/*
	console.log("Alterando documento...");
	const resultado = await colecao.updateOne(
		{_id: new ObjectId("632b4aa18a34bedb2e0810fb")},
		{$set: {idade: 13}},
	);
	
	const resultado2 = await colecao.findOneAndUpdate(
		{nome: "Lilian Carvalho"},
		{$set: {idade: 28}},
	);
	console.log(`Alterado: ${resultado2.value?._id}`);
	*/

	/*
	console.log("Removendo documento");
	const resultado = await colecao.deleteOne({
		_id: new ObjectId("632b4aa18a34bedb2e0810fb"),
	});
	console.log(`Removido: ${resultado.deletedCount}`);
	*/

} catch (error) {
	console.log("Falha de acesso ao MongoDB");
	console.log(error);
} finally {
	await cliente.close();
}

console.log("Fim de programa!");
